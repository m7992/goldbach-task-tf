kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.5/cert-manager.crds.yaml
helm repo add jetstack https://charts.jetstack.io 
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install ingress-nginx ingress-nginx/ingress-nginx
helm install cert-manager --set ingressShim.defaultIssuerKind=Issuer --set ingressShim.defaultIssuerName=letsencrypt --set extraArgs='{--dns01-recursive-nameservers-only,--dns01-recursive-nameservers=8.8.8.8:53,1.1.1.1:53}' jetstack/cert-manager

kubectl create secret generic "digitalocean-dns" --from-literal=access-token=$TF_VAR_do_token
kubectl apply -f ./certbot-scripts/issuer.yml
