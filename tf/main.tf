resource "digitalocean_project" "playground" {
  name        = "simple_testing"
  description = "A simple test-task from goldbach"
  purpose     = "A simple test-task from goldbach"
  environment = "Development"
}

#resource "digitalocean_kubernetes_cluster" "goldbach_prod" {
#  name   = "goldbach-prod"
#  region = "ams3"
#  version = "1.29.1-do.0"
#
#  node_pool {
#    name       = "worker-pool"
#    size       = "s-1vcpu-2gb"
#    node_count = 2
#  }
#}

#resource "digitalocean_firewall" "fw" {
#  name = "only-80-443-inbound-all-outbound"#
#
#  droplet_ids = (digitalocean_kubernetes_cluster.goldbach_prod.node_pool[*].nodes[*].droplet_id)[0]#
#
#  inbound_rule {
#    protocol         = "tcp"
#    port_range       = "80"
#    source_addresses = ["0.0.0.0/0", "::/0"]
#  }#
#
#  inbound_rule {
#    protocol         = "tcp"
#    port_range       = "443"
#    source_addresses = ["0.0.0.0/0", "::/0"]
#  }
#}
